package sample;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Mobil {
    private String tipe;
    private String nomor;
    private LocalDateTime jamMasuk;
    private LocalDateTime jamKeluar;
    private String status;
    public static long hargaPerJam = 1000;

    public Mobil(String tipe, String nomor, LocalDateTime jamMasuk, LocalDateTime jamKeluar, String status) {
        this.tipe = tipe;
        this.nomor = nomor;
        this.jamMasuk = jamMasuk;
        this.jamKeluar = jamKeluar;
        this.status = status;
    }

    public String getTipe() {
        return tipe;
    }

    public String getNomor() {
        return nomor;
    }

    public String getJamMasuk() {
        return jamMasuk.format(DateTimeFormatter.ofPattern("HH:mm:ss dd-MM-yyyy"));
    }

    public String getJamKeluar() {
        return jamKeluar == null
                ? ""
                : jamKeluar.format(DateTimeFormatter.ofPattern("hh:mm:ss dd/MM/yyyy"));
    }

    public String getStatus() {
        return status;
    }

    public String getLamaParkir() {
        return String.valueOf(jamKeluar == null ? 0 : ChronoUnit.HOURS.between(jamMasuk, jamKeluar)) + " Jam";
    }

    public long hitungLamaParkir() {
        return jamKeluar == null ? 0 : ChronoUnit.HOURS.between(jamMasuk, jamKeluar);
    }

    public String getBiayaParkir() {
        return String.valueOf("Rp." + hitungLamaParkir() * hargaPerJam + ",00");
    }

    public void setJamKeluar(LocalDateTime jamKeluar) {
        this.jamKeluar = jamKeluar;
    }


    public void setStatus(String status) {
        this.status = status;
    }
}
