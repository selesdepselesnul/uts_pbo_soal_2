package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class Controller implements Initializable {
    @FXML
    public TableColumn<Mobil, String> tipeMobilCol;
    @FXML
    public TableColumn<Mobil, String> nomorKendaraanCol;
    @FXML
    public TableColumn<Mobil, String> masukCol;
    @FXML
    public TableColumn<Mobil, String> keluarCol;
    @FXML
    public TableColumn<Mobil, String> statusCol;
    @FXML
    public TableColumn<Mobil, String> lamaParkirCol;
    @FXML
    public TableColumn<Mobil, String> biayaParkirCol;
    @FXML
    public TextField nomor;
    @FXML
    public ComboBox<String> tipe;
    @FXML
    public TableView<Mobil> parkirTable;
    @FXML
    public TextField jamMasuk;
    @FXML
    public TextField jamKeluar;
    @FXML
    public Button parkirButton;
    @FXML
    public Label lahanParkir;
    @FXML
    public Label tersedia;
    @FXML
    public Label terpakai;
    @FXML
    public TextField nomorSearching;
    @FXML
    public TextField hargaPerJam;
    private static String MULAI = "Mulai Parkir !";
    private Parkir parkir = new Parkir(3);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tipeMobilCol.setCellValueFactory(new PropertyValueFactory<Mobil, String>("tipe"));
        nomorKendaraanCol.setCellValueFactory(new PropertyValueFactory<Mobil, String>("nomor"));
        masukCol.setCellValueFactory(new PropertyValueFactory<Mobil, String>("jamMasuk"));
        keluarCol.setCellValueFactory(new PropertyValueFactory<Mobil, String>("jamKeluar"));
        statusCol.setCellValueFactory(new PropertyValueFactory<Mobil, String>("status"));
        lamaParkirCol.setCellValueFactory(new PropertyValueFactory<Mobil, String>("lamaParkir"));
        biayaParkirCol.setCellValueFactory(new PropertyValueFactory<Mobil, String>("biayaParkir"));


        parkirButton.setText(MULAI);
        parkirTable
            .getColumns()
            .setAll(tipeMobilCol,
                    nomorKendaraanCol,
                    masukCol,
                    keluarCol,
                    statusCol,
                    lamaParkirCol,
                    biayaParkirCol);

        lahanParkir.setText(parkir.getJumlahTempatParkir());
        terpakai.setText(parkir.getJumlahMobilParkir());
        tersedia.setText(parkir.getJumlahTersedia());
        hargaPerJam.setText(String.valueOf(Mobil.hargaPerJam));
        tipe.getItems().setAll("01", "02", "03");

    }

    @FXML
    public void onSearchingNomor() {

        String searchedNum = nomorSearching.getText();
        ObservableList<Mobil> items = parkirTable.getItems();
        System.out.println(searchedNum);
        if(searchedNum.equals("")) {
            items.setAll(parkir.allMobil);
        } else {
            items.setAll(parkir
                .allMobil
                .stream()
                .filter(x -> x.getNomor().equals(searchedNum))
                .collect(Collectors.toList()));
        }

    }

    private LocalDateTime toLDT(String val) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd-MM-yyyy");
        return LocalDateTime.parse(val, formatter);
    }


    private void clearInput() {
        tipe.getSelectionModel().clearSelection();
        nomor.clear();
        jamMasuk.clear();
        jamKeluar.clear();
    }

    @FXML
    public void onAddParkir(ActionEvent actionEvent) {
        if(parkirButton.getText().equals(MULAI)) {

            Mobil m = new Mobil(tipe.getSelectionModel().getSelectedItem(),
                    nomor.getText(),
                    toLDT(jamMasuk.getText()),
                    null,
                    "Sedang Parkir");

            if(parkir.masukMobil(m)) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Berhasil");
                alert.setHeaderText(null);
                alert.setContentText("Data sudah disimpan boss!");

                alert.showAndWait();

                parkirTable
                    .getItems()
                    .add(m);

            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Penuh Coy");
                alert.setHeaderText("Sudah Penuh");
                alert.setContentText("Lahan parkir sudah penuh boss!");

                alert.showAndWait();
            }
            clearInput();
        } else {
            Mobil selectedMobil = parkirTable
                                    .getItems()
                                    .filtered(x ->
                                            x.getNomor().equals(nomor.getText())
                                            && x.getJamMasuk().equals(jamMasuk.getText()))
                                    .get(0);
            System.out.println(selectedMobil.getNomor());
            parkirTable.getItems().remove(selectedMobil);
            selectedMobil.setJamKeluar(toLDT(jamKeluar.getText()));
            selectedMobil.setStatus("Selesai Parkir");


            parkir.keluarMobil();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Berhasil");
            alert.setHeaderText(null);
            alert.setContentText("Parkiran sudah berkurang boss!");


            alert.showAndWait();

            jamMasuk.setDisable(false);
            nomor.setDisable(false);
            tipe.setDisable(false);

            jamKeluar.setDisable(true);
            parkirButton.setText(MULAI);

            parkirTable.getItems().add(selectedMobil);
            clearInput();
        }

        tersedia.setText(parkir.getJumlahTersedia());
        terpakai.setText(parkir.getJumlahMobilParkir());

    }

    @FXML
    public void onFinishedParking(ActionEvent actionEvent) {
        Mobil selectedMobil = parkirTable
                .getSelectionModel()
                .getSelectedItem();

        jamKeluar.setDisable(false);

        jamMasuk.setText(selectedMobil.getJamMasuk());
        nomor.setText(selectedMobil.getNomor());
        tipe.setValue(selectedMobil.getTipe());

        jamMasuk.setDisable(true);
        nomor.setDisable(true);
        tipe.setDisable(true);

        parkirButton.setText("Selesai Parkir !");
    }

    @FXML
    public void onSimpanHargaPerjam(ActionEvent actionEvent) {
        Mobil.hargaPerJam = Integer.parseInt(hargaPerJam.getText());
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Pergantian Harga");
        alert.setHeaderText("Harga Ganti");
        alert.setContentText("Harga perjam sudah diganti!");

        alert.showAndWait();
    }
}
