package sample;


import java.util.ArrayList;
import java.util.List;

public class Parkir {
    private int jumlahTempatParkir;
    private int jumlahMobilParkir;
    public List<Mobil> allMobil = new ArrayList<>();

    public Parkir(int jumlahTempatParkir) {
        this.jumlahTempatParkir = jumlahTempatParkir;
    }

    public boolean masukMobil(Mobil m) {
        if(jumlahMobilParkir == jumlahTempatParkir)
            return false;

        jumlahMobilParkir++;
        allMobil.add(m);
        return true;
    }

    public void keluarMobil() {
        if(jumlahMobilParkir != 0)
            jumlahMobilParkir--;
    }

    public String getJumlahTempatParkir() {
        return  jumlahTempatParkir + " tempat parkir";
    }

    public String getJumlahMobilParkir() {
        return jumlahMobilParkir + " mobil";
    }

    public String getJumlahTersedia() {
        return (jumlahTempatParkir - jumlahMobilParkir) + " tersedia";
    }
}
